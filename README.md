![](RFID/ArduinoRC522.png)

## Arduino
**Requirments:**
-Arduino Duo
-RC522
-Buzzer
-LED diode
-.Net framework 4.5 or higher

•Connect arduino uno and RC522 with the jumper cables as shown in the image above.
you can also use the fritzing program, the file can be found in **RFID/ArduinoRC522.fzz**

•Upload file **RFIDaccessSystem-Arduino.ino** using [https://www.arduino.cc/en/Main/Software](Arduino IDE).

## Usage
•Change server parameters at **RFID/Mysql.vb**

•Change admin password at RFID/Form1.vb
at line 248     If TextBox1.Text = "admin" AndAlso TextBox6.Text = "admin"

## Copyright and license

Aleksander Karapetian 2018 © **MIT**