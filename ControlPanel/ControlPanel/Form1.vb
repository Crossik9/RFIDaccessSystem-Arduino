﻿Imports MySql.Data.MySqlClient
Imports System.Data.SqlClient
Public Class Form1
    Dim myport As Array

    Dim serial As New clsSerialConfig
    Public retornoSerial, retornoLinha As String
    Dim serialBuffer, serialLinha As String
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        Using SQLConnection As New MySqlConnection("server=127.0.0.1;user id=admin;password=admin;database=test")
            Using sqlCommand As New MySqlCommand()
                With sqlCommand
                    .CommandText = "INSERT INTO workers (`First Name`,`Last Name`,`RFID`,`Phone Number`,`Status`) values (@1Name, @2Name, @RFID, @Phone, @status)"
                    .Connection = SQLConnection
                    .CommandType = CommandType.Text
                    .Parameters.AddWithValue("@1Name", TextBox1.Text)
                    .Parameters.AddWithValue("@2name", TextBox4.Text)
                    .Parameters.AddWithValue("@RFID", TextBox2.Text)
                    .Parameters.AddWithValue("@Phone", TextBox5.Text)
                    .Parameters.AddWithValue("@status", TextBox3.Text)

                End With

                Try

                    SQLConnection.Open()
                    sqlCommand.ExecuteNonQuery()


                Catch ex As MySqlException
                    MsgBox(ex.Message.ToString)

                Finally
                    SQLConnection.Close()
                End Try
            End Using
        End Using



    End Sub
    Private Sub CarregarTaxaEPortas()
        'wyykrywanie serial portu
        myport = IO.Ports.SerialPort.GetPortNames
        For i = 0 To UBound(myport)
            cboPorta.Items.Add(myport(i))
        Next

        'resetowanie comboboxow do index0

        cboPorta.SelectedIndex = 0
        cboTaxa.SelectedIndex = 0

    End Sub
    Sub connect()
        If serial.Conectar Then
            btnConectar.Enabled = False
            btnDesconectar.Enabled = True
            cboPorta.Enabled = False
            cboTaxa.Enabled = False
            tmrGravarPessoa.Enabled = True
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        '    CarregarTaxaEPortas()
        ' connect()

        ComboBox1.Items.Add("test")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim str = "server=127.0.0.1;user id=admin;password=admin;database=test"
        Dim sqlQuery As String = "DELETE FROM workers WHERE RFID=@id"
        Using con = New MySqlConnection(str)
            Using cmd = New MySqlCommand(sqlQuery, con)
                con.Open()

                With cmd
                    .Parameters.AddWithValue("@id", TextBox2.Text)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using

    End Sub


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Call Connection()
        Try
            strSql = "SELECT * FROM workers;"
            cmd.CommandText = strSql
            cmd.Connection = conn
            dadapter.SelectCommand = cmd

            datardr = cmd.ExecuteReader
            If datardr.HasRows Then
                datardr.Read()
                TextBox2.Text = datardr("RFID")
                TextBox1.Text = datardr("First Name")
                TextBox4.Text = datardr("Last Name")
                TextBox5.Text = datardr("Phone Number")
                TextBox3.Text = datardr("Status")
            End If

            conn.Close()
        Catch ex As Exception

        End Try




    End Sub
    Public conn As New MySqlConnection
    Public cmd As New MySqlCommand
    Public dadapter As New MySqlDataAdapter
    Public datardr As MySqlDataReader
    Public strSql As String

    Public Sub Connection()
        conn.ConnectionString = "server=127.0.0.1;user id=admin;password=admin;database=test"
        conn.Open()


    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles btnConectar.Click
        connect()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles btnDesconectar.Click
        If serial.Desconectar Then
            btnConectar.Enabled = True
            btnDesconectar.Enabled = False
            cboPorta.Enabled = True
            cboTaxa.Enabled = True
            tmrGravarPessoa.Enabled = False
            tmrBuscarPessoa.Enabled = False
        End If
    End Sub

    Private Sub tmrGravarPessoa_Tick(sender As Object, e As EventArgs) Handles tmrGravarPessoa.Tick
        TextBox2.Text = LerTag()
    End Sub

    Private Function LerTag() As String
        Dim tag As String = ""
        tag = serial.LerLinha

        If Not IsNothing(tag) Then
            tag = tag.Trim

            If Len(tag) < 7 Then
                LerTag = ""
            Else
                LerTag = tag
            End If

        End If
    End Function
End Class
