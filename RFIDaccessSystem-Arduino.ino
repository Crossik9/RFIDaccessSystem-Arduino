#include <SPI.h>
#include <MFRC522.h>

 
#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);
 
char st[20];
int ledPin = 2; 
int ledPin2 = 3; 
const int buzzer = 4;
 
void setup() 
{
  Serial.begin(9600); 
  SPI.begin();     
  mfrc522.PCD_Init();  
  pinMode(ledPin, OUTPUT); 
   pinMode(ledPin2, OUTPUT);
  digitalWrite(ledPin, LOW); 
  pinMode(buzzer, OUTPUT);
}

 
void loop() {
   info();
 karta();
}


void karta(){
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  //Mostra UID na serial
  String conteudo= "";
  byte letra;
  for (byte i = 0; i < mfrc522.uid.size; i++) {
     conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
     conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
    conteudo.toUpperCase();
  Serial.println(conteudo.substring(1)); //UID 1 - Chaveiro
}


void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
}   


 

void info(){

int val = Serial.read() - '0';

if (val == 1) { 
digitalWrite(ledPin, HIGH); 
}
else if (val == 0) 
{
digitalWrite(ledPin, LOW); 
}
else if (val == 2) 
{
digitalWrite(ledPin2, HIGH); 
}
else if (val == 3) 
{
digitalWrite(ledPin2, LOW); 
}

else if (val == 4)
{
  tone(buzzer, 100); 
  delay(500);        
  tone(buzzer, 100); 
  delay(500);          
  noTone(buzzer);    
  delay(500);    
}


else if (val == 5) 
{
  tone(buzzer, 900); 
  delay(150); 
  noTone(buzzer);  
  delay(150);      
  tone(buzzer, 1000); 
  delay(150);          
  noTone(buzzer);    
}
else if (val == 7) 
{
  tone(buzzer,1000); 
  delay(200);          
  noTone(buzzer);    
  delay(500);  
}
else if (val == 6) 
{
  serialFlush();
}
else 
{
}
}
 

